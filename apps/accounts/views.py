from django.shortcuts import render
from django.views.generic import TemplateView
from django.views import generic
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
class LoginView(TemplateView):
    template_name = 'accounts/login.html'

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(AddSkillView, self).dispatch(request, *args, **kwargs)

class ForgotView(TemplateView):
    template_name = 'accounts/forgot.html'

class RegisterView(TemplateView):
    template_name = 'accounts/register.html'

class RegisterNextView(TemplateView):
    template_name = 'accounts/second_registration.html'
