# from django import forms
# from django.forms import ModelForm
# from login.models import SignUp
from django import forms
# #from django import forms
from django.contrib.auth import forms as auth_forms


class EntryForm(auth_forms.AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(EntryForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update(
            {
            'class': 'auto-focus form-control',
            'placeholder':'username',
            }
        )
        self.fields['password'].widget.attrs.update(
            {
            'class' : 'form-control',
            'placeholder' : 'Password',
            }
        )