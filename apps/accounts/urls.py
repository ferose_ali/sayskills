from django.conf.urls import url
from . import forms
from apps.accounts.forms import EntryForm
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    # url(r'^$', views.LoginView.as_view(), name='login'),
    url(r'^$', auth_views.login,{
    'template_name':'accounts/login.html', 'authentication_form': forms.EntryForm}, name='login'),
    url(r'^accounts/forgot/', views.ForgotView.as_view(), name='forgot'),
    url(r'^accounts/register/', views.RegisterView.as_view(), name='register'),
    url(r'^accounts/register_step2/', views.RegisterNextView.as_view(), name='register2'),
]

