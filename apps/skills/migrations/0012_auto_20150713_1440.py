# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0011_auto_20150713_1047'),
    ]

    operations = [
        migrations.AlterField(
            model_name='managercriteriarating',
            name='starrate',
            field=models.IntegerField(default=0),
        ),
    ]
