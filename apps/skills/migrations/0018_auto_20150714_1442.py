# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0017_auto_20150714_1441'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userskill',
            name='endorse_count',
            field=models.IntegerField(default=0, blank=True),
        ),
    ]
