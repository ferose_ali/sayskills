# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0003_auto_20150703_1430'),
    ]

    operations = [
        migrations.RenameField(
            model_name='managerreview',
            old_name='userid',
            new_name='user',
        )
    ]
