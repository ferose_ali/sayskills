# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0005_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userskill',
            name='endorse_count',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userskill',
            name='user',
            field=models.ForeignKey(related_name=b'user_userskills', to=settings.AUTH_USER_MODEL),
        ),
    ]
