# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0009_auto_20150713_1001'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewrequest',
            name='requested_on',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
    ]
