# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0015_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userskill',
            name='avg_skill_rate',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='userskill',
            name='endorse_count',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
