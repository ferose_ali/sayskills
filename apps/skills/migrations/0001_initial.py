# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Endorse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('endorse_date', models.DateTimeField(auto_now_add=True)),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Endorses',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MainSkill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('skill_name', models.CharField(max_length=100, verbose_name=b'Skill Name')),
                ('added_on', models.DateTimeField(auto_now_add=True)),
                ('skill_description', models.CharField(max_length=200, null=True, verbose_name=b'Skill Description', blank=True)),
            ],
            options={
                'verbose_name_plural': 'MainSkills',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MainSkillCriteria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('criteria', models.CharField(max_length=144, verbose_name=b'Skill Rating Criteria')),
                ('main_skill', models.ForeignKey(to='skills.MainSkill')),
            ],
            options={
                'verbose_name_plural': 'MainSkillCriterias',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ManagerRating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.FloatField()),
                ('main_skill', models.ForeignKey(to='skills.MainSkill')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ManagerReview',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('project_name', models.CharField(max_length=144, verbose_name=b'Project Name')),
                ('comment', models.CharField(max_length=250, verbose_name=b'Comments')),
                ('average_rate', models.FloatField()),
                ('added_by', models.ForeignKey(related_name=b'manager', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReviewRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=None, max_length=10, null=True, choices=[(b'0', b'Requested'), (b'1', b'Accepted'), (b'-1', b'Rejected')])),
                ('to_user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'ReviewRequests',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubSkill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subskill_name', models.CharField(max_length=100, verbose_name=b'Subskill Name')),
                ('added_on', models.DateTimeField(auto_now_add=True)),
                ('subskill_description', models.CharField(max_length=200, null=True, verbose_name=b'Subskill Description', blank=True)),
                ('mainskill', models.ForeignKey(to='skills.MainSkill')),
            ],
            options={
                'verbose_name_plural': 'SubSkills',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserMainSkillRate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('overall_skill_rate', models.FloatField()),
                ('main_skill', models.ForeignKey(to='skills.MainSkill')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserSkill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('added_date', models.DateTimeField(auto_now_add=True)),
                ('endorse_count', models.IntegerField(null=True, blank=True)),
                ('avg_skill_rate', models.FloatField(null=True, blank=True)),
                ('rated_by', models.ForeignKey(related_name=b'get_manager', to=settings.AUTH_USER_MODEL)),
                ('sub_skill', models.ForeignKey(to='skills.SubSkill')),
                ('userid', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'UserSkills',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='reviewrequest',
            name='user_skill',
            field=models.ForeignKey(to='skills.UserSkill'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='managerreview',
            name='sub_skill',
            field=models.ForeignKey(to='skills.SubSkill'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='managerreview',
            name='userid',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='managerrating',
            name='manager_review',
            field=models.ForeignKey(to='skills.ManagerReview'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='endorse',
            name='user_skill',
            field=models.ForeignKey(to='skills.UserSkill'),
            preserve_default=True,
        ),
    ]
