# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0003_auto_20150708_1050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userskill',
            name='endorse_count',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='userskill',
            name='user',
            field=models.ForeignKey(related_name=b'user_skills', to=settings.AUTH_USER_MODEL),
        ),
    ]
