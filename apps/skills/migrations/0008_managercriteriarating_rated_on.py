# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0007_managercriteriarating'),
    ]

    operations = [
        migrations.AddField(
            model_name='managercriteriarating',
            name='rated_on',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
    ]
