# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0013_auto_20150714_1127'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='managerrating',
            name='main_skill',
        ),
        migrations.RemoveField(
            model_name='managerrating',
            name='manager_review',
        ),
        migrations.DeleteModel(
            name='ManagerRating',
        ),
        migrations.RemoveField(
            model_name='userskill',
            name='rated_by',
        ),
    ]
