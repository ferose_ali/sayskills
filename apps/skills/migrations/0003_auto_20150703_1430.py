# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0002_auto_20150703_1422'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userskill',
            name='rated_by',
            field=models.ForeignKey(related_name=b'get_manager', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
