# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0018_auto_20150714_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usermainskillrate',
            name='overall_skill_rate',
            field=models.FloatField(default=0.0),
        ),
    ]
