# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0013_auto_20150714_0933'),
        ('skills', '0014_auto_20150714_1215'),
    ]

    operations = [
    ]
