# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0008_managercriteriarating_rated_on'),
    ]

    operations = [
        migrations.AlterField(
            model_name='managercriteriarating',
            name='rated_on',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
