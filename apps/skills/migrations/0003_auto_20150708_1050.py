# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0002_auto_20150708_1049'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userskill',
            old_name='userid',
            new_name='user',
        ),
    ]
