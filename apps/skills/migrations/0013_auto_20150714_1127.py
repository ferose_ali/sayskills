# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0012_auto_20150713_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userskill',
            name='avg_skill_rate',
            field=models.FloatField(),
        ),
    ]
