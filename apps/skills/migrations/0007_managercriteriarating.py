# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('skills', '0006_auto_20150710_1303'),
    ]

    operations = [
        migrations.CreateModel(
            name='ManagerCriteriaRating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('starrate', models.IntegerField(null=True)),
                ('byuser', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('mcriteria', models.ForeignKey(to='skills.MainSkillCriteria')),
                ('uskill', models.ForeignKey(to='skills.UserSkill')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
