import json
#from notifications import notify

from django.shortcuts import render, redirect
from django.views.generic import TemplateView,View
from django.views import generic
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User, Group
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Avg
from models import SubSkill,UserSkill,MainSkill,ReviewRequest,MainSkillCriteria,ManagerCriteriaRating,ManagerReview,UserMainSkillRate,Endorse

# Create your views here.
class ProfileView(TemplateView):
    template_name = 'skills/home.html'

    def get(self, request, *args, **kwargs):
        context = {}
        if request.user.groups.filter(name = 'Manager').exists() :
            return redirect('manager')

        manager_review = ManagerReview.objects.filter(user=request.user)
        commentlist = []
        for i in manager_review:
            commentlist.append({
                'skill':i.sub_skill.id,
                'comment':i.comment,
                'projectnm':i.project_name,
                'addedby':i.added_by.username
            })

        try:
            rating = UserMainSkillRate.objects.get(user=request.user)
            rates = (rating.overall_skill_rate * 10)
            print "rates*************",rates
        except UserMainSkillRate.DoesNotExist:
            rates = 0    
        # userskill = request.user.user_userskills.all()
        # manager_review = ManagerView.objects.get(user=request.user, sub_skill=userskill)
        # print manager_review
        # manager_comment = "HAi man How are You"
    	context['name'] = request.user.username
    	context['email'] = request.user.email
    	context['fname'] = request.user.first_name
    	context['lname'] = request.user.last_name
        context['ratings'] = rates
        context['skill'] = request.user.user_userskills.all()
        context['comments'] = commentlist
        #context['mainskills'] = MainSkill.objects.all()
        context['manages'] = User.objects.filter(groups__name="Manager")
        return render(request, self.template_name, context)

class ColleaguesView(TemplateView):
    template_name = 'skills/colleagues.html'

    def get(self, request, *args, **kwargs):
        context = {}
        context['name'] = request.user.username
        context['email'] = request.user.email
        context['fname'] = request.user.first_name
        context['lname'] = request.user.last_name
        currnt_usrid = request.user.id
        supr_usr_id = User.objects.get(is_superuser=True).id
        object_id_list = [currnt_usrid, supr_usr_id]
        context['numbers'] = User.objects.exclude(id__in = object_id_list).count()
        context['all_users'] = User.objects.exclude(id__in = object_id_list)
        return render(request, self.template_name, context)


class ManagerView(TemplateView):
    template_name = 'skills/manager.html'

    def get(self, request, *args, **kwargs):
        context = {}
        context['name'] = request.user.username
    	context['email'] = request.user.email
    	context['fname'] = request.user.first_name
    	context['lname'] = request.user.last_name
        if request.user.groups.filter(name = 'Manager').exists():
            return render(request, self.template_name, context)
        else:
            return redirect('profile')

class SkillSuggestionView(View):

    def get(self, request, *args, **kwargs):
        kin = request.GET.get('k')
        subskills = SubSkill.objects.filter(subskill_name__istartswith = kin)
        sub_skill_lists = []
        for skills in subskills:
              sub_skill_lists.append(skills.subskill_name)
        context = dict()
        context['skills'] = sub_skill_lists
        return HttpResponse(json.dumps(context), content_type='application/x-json')


class FriendsView(TemplateView):
   template_name = 'skills/friends.html'
   def get(self, request, evt):
       obj = User.objects.get(pk=evt)
       userskill = obj.user_userskills.all()
       context = {}
       sub_skill_lists = []
       if userskill:
           for skills in userskill:
               sub_skill_lists.append(skills.sub_skill)
       try:
           rating = UserMainSkillRate.objects.get(user=obj)
           rates = (rating.overall_skill_rate * 10)
       except UserMainSkillRate.DoesNotExist:
           rates = 0
       context['name'] = request.user.username
       context['email'] = request.user.email
       context['fname'] = request.user.first_name
       context['lname'] = request.user.last_name
       context['f_email'] = obj.email
       context['f_first_name'] = obj.first_name
       context['f_last_name'] = obj.last_name
       context['f_subskill'] = userskill
       context['ratings'] = rates
       context['mainskills'] = MainSkill.objects.all()
       return render(request, self.template_name, context)    



class ManagerNotificationView(View):

    def get(self, request, *args, **kwargs):
        revRequests = ReviewRequest.objects.filter(status=0, to_user=request.user.id).order_by('-requested_on')
        notificationsdata = []
        for req in revRequests:
            notificationsdata.append({
                'formUser':req.user_skill.user.username,
                'skill':req.user_skill.sub_skill.subskill_name,
                'skillid':req.user_skill.id,
                'mskill_id':req.user_skill.sub_skill.mainskill.id,
                })
        context = {}
        count = revRequests.count()
        context['htmldata'] = render_to_string('skills/notifications.html',{"count":count, "review_request":notificationsdata})
        context['count'] = count
        return HttpResponse(json.dumps(context), content_type='application/x-json')


class GetCriterias(TemplateView):
    template_name = 'skills/review.html'
    def get(self, request, *args, **kwargs):
        criterias = []
        context = {}
        uskill_id = kwargs.get('id')
        context['userskillid'] = uskill_id
        usk = UserSkill.objects.get(id=uskill_id)
        by = User.objects.get(id=usk.user_id)
        context['username'] = by.username
        nm = SubSkill.objects.get(id=usk.sub_skill_id)
        context['skillname'] = nm.subskill_name
        context['mainskill'] = nm.mainskill
        get_criteria = MainSkillCriteria.objects.filter(main_skill=nm.mainskill)
        for gc in get_criteria:
            print "**************",gc.id
            criterias.append({
                'mcriteriaid':gc.id,
                'mcriteria':gc.criteria,
                })
        context['userskillid'] = uskill_id
        context['criterias'] = criterias
        context['totalcriteria'] = len(criterias)
        context['name'] = request.user.username
        context['email'] = request.user.email
        context['fname'] = request.user.first_name
        context['lname'] = request.user.last_name
        if request.user.groups.filter(name = 'Manager').exists():
            return render(request, self.template_name, context)
        else:
            return redirect('profile')

        #return render(request, self.template_name, {'userskillid':uskill_id,'username':by.username,'skillname':nm.subskill_name,'mainskill':nm.mainskill,'criterias':criterias})


class AddSkillView(TemplateView):
    template_name = 'skills/home.html'
    model = UserSkill


    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(AddSkillView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        user_skill = request.POST.get('skill-text')
        ee = request.user
        t = SubSkill.objects.filter(subskill_name__iexact=user_skill)
        if t:
            t = t[0]
            pp = UserSkill.objects.filter(user=ee, sub_skill=t).exists()
            if pp :
                print "Object Exists"
            else:
                u = UserSkill()
                u.user = request.user
                u.sub_skill = t
                # u = SubSkill.objects.create(userid = name, sub_skill = t)
                u.save()
                return HttpResponse(json.dumps({'success': 'success'}), content_type='application/x-json')


class GetSkillView(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(GetSkillView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        ee = request.user
        t = UserSkill.objects.filter(user=ee)
        manages = User.objects.filter(groups__name="Manager")
        context = dict()
        count = t.count()
        # print count
        sub_skill_lists = []
        if t:
            for skills in t:
                sub_skill_lists.append(skills.sub_skill)
        #print "The Count is .........", count
        #print type(count)
        manager_review = ManagerReview.objects.filter(user=request.user)
        commentlist = []
        for i in manager_review:
            commentlist.append({
                'skill':i.sub_skill.id,
                'comment':i.comment,
                'projectnm':i.project_name,
                'addedby':i.added_by.username
            })
        context['message'] = render_to_string('skills/activation_skill.html', {'skill': t, 'manages': manages,'comments':commentlist})
        #print context['message'], "Message 4444444444444444444444444"
        # # res = {'html': message}
        return HttpResponse(json.dumps(context), content_type='application/x-json')

class NotificationRequest(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(NotificationRequest, self).dispatch(request, *args, **kwargs)

    def post(self, request):

        managerid = request.POST.get('mgr')
        mm = request.POST.get('sname')
        currentuser = request.user
        print managerid, "&&&&&&&&&&&&&&&&&&&&&&"
        manager = User.objects.get(id__iexact=managerid)
        print manager, "Manager Object is ----------------"
        sobject = SubSkill.objects.get(subskill_name__iexact=mm)
        ukill = UserSkill.objects.get(user=currentuser, sub_skill=sobject)
        UserSkill.objects.filter()
        if ReviewRequest.objects.filter(to_user=managerid, status=0, user_skill=ukill).exists():
            print "Failed"
        else:
            noti = ReviewRequest()
            noti.user_skill = ukill
            noti.to_user = manager
            noti.status = 0
            print"-----------------------------------------------------------------"
            print noti.user_skill, "SKILL", noti.to_user, "Status", noti.status
            print"-----------------------------------------------------------------"
            noti.save()
            #notify.send(currentuser, recipient=manager, verb='Review Request', description="I had sent a request to rate my current skill")
            return HttpResponse(json.dumps({'v': 'f'}), content_type='application/x-json')
            # ukill.delete()


class DeleteSkill(View) :

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteSkill, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        print "Welcome"
        usid = request.POST.get('userskill_id')
        print usid, "**********************************"
        u_skl = UserSkill.objects.get(id = usid)
        s_sklinstance = SubSkill.objects.get(id = u_skl.sub_skill.id)
        if ManagerReview.objects.filter(user=request.user,sub_skill=s_sklinstance).exists():
            print "####------> True"

            # ManagerReview.objects.get(user=request.user,sub_skill=s_sklinstance).delete()
            # u_skl.delete()
            return HttpResponse(json.dumps({'status': 'You have not a provision to delete the manager rated skill'}), content_type='application/x-json')
        else:
            print "#### --> False" 
            u_skl.delete()
            return HttpResponse(json.dumps({'status': 'Your Skill Successfully Deleted'}), content_type='application/x-json')
        print "@@@@@@ ----> ",s_sklinstance 

        if UserMainSkillRate.objects.filter(user = request.user).exists():
            print "User Main skill exixts"
            usr_skl = UserSkill.objects.filter(user = request.user).aggregate(mavg_rate=Avg('avg_skill_rate'))
            print "MAverage --->",usr_skl.get('mavg_rate')
            if usr_skl.get('mavg_rate',0.0) is not None:
                avg_all_mskillrate = round(usr_skl.get('mavg_rate',0.0),2)
                print "Average After Deletion -------------> ",avg_all_mskillrate
                umrate = UserMainSkillRate.objects.get(user = request.user)
                umrate.overall_skill_rate = avg_all_mskillrate
                umrate.save()
            else:
                count = UserSkill.objects.filter(user = request.user).count()
                print "Count --> ",count
                if count >= 1:
                    umrate = UserMainSkillRate.objects.get(user = request.user)
                    umrate.overall_skill_rate = 0.0
                    umrate.save()
                else:
                    umrate = UserMainSkillRate.objects.get(user = request.user).delete()
        else:
            print "Not exists"

        


class AddStarRatingView(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(AddStarRatingView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
       if request.is_ajax():
            uid = request.POST.get('userid')
            scount = request.POST.get('starcount')
            cid = request.POST.get('criteriaid')
            sid = request.POST.get('skillid')

            print "***************> ",uid
            print "###############> ",scount
            print "---------------> ",cid
            print "_______________> ",sid

            ukill = UserSkill.objects.get(id=sid)
            mcriteria = MainSkillCriteria.objects.get(id=cid)
            usrid = User.objects.get(id=uid)
            if ManagerCriteriaRating.objects.filter(uskill=ukill, mcriteria=mcriteria, byuser = usrid).exists():
                print "exists"
                mcrating = ManagerCriteriaRating.objects.get(byuser=usrid,uskill=ukill,mcriteria=mcriteria)
                mcrating.starrate = scount
                mcrating.save()
                # rates = ManagerCriteriaRating.objects.get_or_create(byuser = usrid,mcriteria = mcriteria,uskill = ukill)
                #rates.save()
            else:
                rates = ManagerCriteriaRating(starrate = scount,byuser = usrid,mcriteria = mcriteria,uskill = ukill)
                rates.save()

            return HttpResponse(json.dumps({'v': 'f'}), content_type='application/x-json')


class GetAllRequests(TemplateView):
    template_name = 'skills/allrequests.html'
    def get(self, request, *args, **kwargs):
        context = {}
        allrequestdata = []
        context['name'] = request.user.username
        context['email'] = request.user.email
        context['fname'] = request.user.first_name
        context['lname'] = request.user.last_name
        manager = request.user.id
        print manager
        revRequests = ReviewRequest.objects.filter(to_user = manager).exclude(status='-1').order_by('-requested_on')

        for ar in revRequests:
            print "**************",ar.id
            allrequestdata.append({
                'reqUserId':ar.user_skill.user.id,
                'reqUserName':ar.user_skill.user.username,
                'reqSubSkill':ar.user_skill.id,
                'mskill':ar.user_skill.sub_skill.mainskill.skill_name,
                'skill':ar.user_skill.sub_skill.subskill_name,
                'skillid':ar.user_skill.id,
                'status':ar.status,
                'requestedon':ar.requested_on
                })

        context['userinfo'] = allrequestdata
        print "~~~~~~~~~~~~~~~~~",allrequestdata
        print "***** count ----->",revRequests.count()
        context['totalcount'] = revRequests.count()
        if request.user.groups.filter(name = 'Manager').exists():
            return render(request, self.template_name, context)
        else:
            return redirect('profile')


class EndorseView(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(EndorseView, self).dispatch(request, *args, **kwargs)
    def post(self,request):
        #print "Entered in to the view"
        firstname = request.POST.get('fname')
        friendskill = request.POST.get('fsubskill')
        operation = request.POST.get('ops')
        print "1st", "Required Operation is", operation
        friend = User.objects.get(first_name__iexact=firstname)
        currentuser = request.user
        userskill = friend.user_userskills.filter(sub_skill__subskill_name__iexact=friendskill)[0]
        if Endorse.objects.filter(added_by=currentuser, user_skill=userskill).exists():
            print "Exists"
        else:
            print "********", userskill
            if(operation == "add"):
                userskill.endorse_count = userskill.endorse_count + 1
            else:
                userskill.endorse_count = userskill.endorse_count - 1
            print userskill.endorse_count
            Endorse.objects.create(user_skill=userskill, added_by=currentuser)
            counts = userskill.endorse_count
            userskill.save()
            context = {}
            context['endorsecount'] = counts
            return HttpResponse(json.dumps(context), content_type='application/x-json')

class AddManagerReview(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(AddManagerReview, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            criteriacount = int(request.POST.get('tcriteria'))
            pname = request.POST.get('projectname')
            rcomment = request.POST.get('comment')
            uskid = request.POST.get('uskid')
            avgrate = float(request.POST.get('avgstarrate'))

            usrskillinstance = UserSkill.objects.get(id=uskid)
            formuserid = usrskillinstance.user.id
            print "USER ID -----> ",formuserid
            formuserskill = usrskillinstance.sub_skill.subskill_name
            formuserskillid = usrskillinstance.sub_skill.id
            userinst = User.objects.get(id=formuserid)
            print "USER INSTANCE -----> ",userinst          
            manager = request.user.id
            sskillinstance = SubSkill.objects.get(id=formuserskillid)

            #usrskillinstance = UserSkill.objects.get(id=uskid)
            print "USER SKILL INSTANCE -----> ",
            usrinstance = User.objects.get(id=manager)
            msid = sskillinstance.mainskill.id
            mainskillinstance = MainSkill.objects.get(id=msid)
            total = 0;
            staravg = 0;
            for i in range(1,criteriacount+1):
                varname = 'hnm%s'%i
                scount = request.POST.get(varname)
                varcid = 'cid%s'%i
                skillcid = request.POST.get(varcid)

                mcriteriainstance = MainSkillCriteria.objects.get(id=skillcid)
                if ManagerCriteriaRating.objects.filter(uskill=usrskillinstance, mcriteria=mcriteriainstance, byuser = usrinstance).exists():
                    #print "exists"
                    mcrating = ManagerCriteriaRating.objects.get(byuser=usrinstance,uskill=usrskillinstance,mcriteria=mcriteriainstance)
                    mcrating.starrate = scount
                    mcrating.save()
                else:                
                    rates = ManagerCriteriaRating(starrate = scount,byuser = usrinstance,mcriteria = mcriteriainstance,uskill = usrskillinstance)
                    rates.save()

            mreview = ManagerReview(user=userinst,sub_skill=sskillinstance,project_name=pname,comment=rcomment,added_by=usrinstance,average_rate=avgrate)
            mreview.save()

            mngrinstance = ManagerReview.objects.get(id=mreview.id)

            mreview_avg = ManagerReview.objects.filter(user=userinst,sub_skill=sskillinstance).aggregate(av_rate=Avg('average_rate'))
            usr_skl = UserSkill.objects.get(user=userinst,sub_skill=sskillinstance)
            usr_skl.avg_skill_rate=round(mreview_avg.get('av_rate', 0.0),2)
            usr_skl.save()


            s_skills = SubSkill.objects.filter(mainskill=mainskillinstance)
            u_skill = UserSkill.objects.filter(user=userinst,sub_skill=s_skills).aggregate(mavg_rate=Avg('avg_skill_rate'))
            
            print "mAverage -------------> ",round(u_skill.get('mavg_rate',0.0),2)
            avg_all_mskillrate = round(u_skill.get('mavg_rate',0.0),2)
            print "Rounded Average ------> ",avg_all_mskillrate

            if UserMainSkillRate.objects.filter(user=userinst, main_skill=mainskillinstance).exists():
                user_mskillrate = UserMainSkillRate.objects.get(user=userinst, main_skill=mainskillinstance)
                user_mskillrate.overall_skill_rate = avg_all_mskillrate
                user_mskillrate.save()                
            else:    
                user_mskillrate = UserMainSkillRate(user=userinst,main_skill=mainskillinstance,overall_skill_rate=avg_all_mskillrate)
                user_mskillrate.save()

            rReq = ReviewRequest.objects.get(user_skill=uskid,status=0,to_user=manager)
            rReq.status = 1
            rReq.save()

        return HttpResponse(json.dumps({'status': 'success'}), content_type='application/x-json')
