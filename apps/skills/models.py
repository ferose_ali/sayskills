from django.db import models
import datetime
from django.utils import timezone
from django.contrib.auth.models import User
# Create your models here.

class MainSkill(models.Model):
    skill_name = models.CharField('Skill Name',max_length=100)
    added_on = models.DateTimeField(auto_now_add=True, blank=True)
    skill_description = models.CharField('Skill Description',max_length=200, null = True, blank = True)

    class Meta:
    	verbose_name_plural="MainSkills"    
    def __str__(self):
        return self.skill_name

class SubSkill(models.Model):
    mainskill = models.ForeignKey(MainSkill)
    subskill_name = models.CharField('Subskill Name',max_length=100)
    added_on = models.DateTimeField(auto_now_add=True, blank=True)
    subskill_description = models.CharField('Subskill Description', max_length=200, null = True, blank = True)

    class Meta:
    	verbose_name_plural="SubSkills"        
    def __str__(self):
        return self.subskill_name        


class UserSkill(models.Model):
    # def __str__(self):
    #     return self.sub_skill.subskill_name
    user = models.ForeignKey(User, related_name="user_userskills")
    sub_skill = models.ForeignKey(SubSkill)
    added_date = models.DateTimeField(auto_now_add=True, blank=True)
    endorse_count = models.IntegerField(default=0,blank=True)
    avg_skill_rate = models.FloatField(default=0.0)

    class Meta:
        verbose_name_plural="UserSkills"
    def __unicode__(self):
        return "%s - %s" % (self.user, self.sub_skill)
    # def __str__(self):
    #     return self.sub_skill.subskill_name

class Endorse(models.Model):
    user_skill = models.ForeignKey(UserSkill)
    added_by = models.ForeignKey(User)
    endorse_date = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        verbose_name_plural="Endorses"
    def __str__(self):
        return self.added_by

class ReviewRequest(models.Model):
    STATUS_CHOICES = (('0', 'Requested'),('1', 'Accepted'),('-1','Rejected'))
    user_skill = models.ForeignKey(UserSkill)
    to_user = models.ForeignKey(User)
    status = models.CharField(max_length=10, choices = STATUS_CHOICES, default = None, null=True)
    requested_on = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    class Meta:
        verbose_name_plural="ReviewRequests"
   # def __unicode__(self):
   #     return "%s"%self.user_skill.sub_skill.subskill_name

class MainSkillCriteria(models.Model):
    main_skill = models.ForeignKey(MainSkill)
    criteria = models.CharField("Skill Rating Criteria", max_length=144)

    class Meta:
        verbose_name_plural="MainSkillCriterias"
    def __str__(self):
        return self.criteria

class UserMainSkillRate(models.Model):
    user = models.ForeignKey(User)
    main_skill = models.ForeignKey(MainSkill)
    overall_skill_rate = models.FloatField(default=0.0)

    # def __str__(self):
    #     return self.overall_skill_rate

class ManagerReview(models.Model):
    user = models.ForeignKey(User)
    sub_skill = models.ForeignKey(SubSkill, related_name='comments')
    project_name = models.CharField('Project Name', max_length=144)
    comment = models.CharField('Comments', max_length=250)
    added_by = models.ForeignKey(User,related_name='manager')
    average_rate = models.FloatField()

    # def __str__(self):
    #     return self.average_rate

class ManagerCriteriaRating( models.Model):
    uskill = models.ForeignKey(UserSkill)
    mcriteria = models.ForeignKey(MainSkillCriteria)
    byuser = models.ForeignKey(User)
    starrate = models.IntegerField(default=0)
    rated_on = models.DateTimeField(auto_now_add=True, blank=True)
