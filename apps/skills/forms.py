from djnago.forms import ModelForm
from apps.skills.models import SubSkill
from django import forms


class AddSkillForm(ModelForm) :
    class meta :
        model = SubSkill
        fields = ('subskill_name')

# class ManagerReviewForm(ModelForm) :
#     class meta :
#         model = ManagerReview
#         fields = ('project_name','comment')
