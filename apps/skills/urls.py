from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^$',login_required(views.ProfileView.as_view()), name='profile'),
    url(r'^logout/$',auth_views.logout,{
    'next_page':'/'}, name='logout'),
    url(r'^colleagues/$', login_required(views.ColleaguesView.as_view()), name='colleagues'),
    url(r'^manager/', login_required(views.ManagerView.as_view()), name='manager'),
    url(r'^suggestion/', login_required(views.SkillSuggestionView.as_view()), name='suggestion'),
    url(r'^friends/(?P<evt>\d+)/$', login_required(views.FriendsView.as_view()), name='friends'),
    url(r'^notifications/', login_required(views.ManagerNotificationView.as_view()), name='notifycount'),
    url(r'^criteria/(?P<id>\d+)/$', login_required(views.GetCriterias.as_view()), name='getcriteria'),
    url(r'^addskill/', login_required(views.AddSkillView.as_view()), name='addskill'),
    url(r'^skillview/', login_required(views.GetSkillView.as_view()), name='skillview'),
    url(r'^sendnotification/', login_required(views.NotificationRequest.as_view()), name='sendnotification'),
    url(r'^deleteskill/', login_required(views.DeleteSkill.as_view()), name='deleteskill'),
    url(r'^starrating/', login_required(views.AddStarRatingView.as_view()), name='starrating'),
    url(r'^all/$', login_required(views.GetAllRequests.as_view()), name='getallrequests'),
    url(r'^add_review/$', login_required(views.AddManagerReview.as_view()), name='addreview'),    
    url(r'^endorse/', login_required(views.EndorseView.as_view()), name='endorse')
]
