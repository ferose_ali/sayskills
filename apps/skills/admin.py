from django.contrib import admin

from models import MainSkill
from models import SubSkill
from models import MainSkillCriteria



admin.site.register(MainSkill)
admin.site.register(SubSkill)
admin.site.register(MainSkillCriteria)


