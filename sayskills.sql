-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 14, 2015 at 09:20 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sayskills`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `auth_group`
--

INSERT INTO `auth_group` (`id`, `name`) VALUES
(1, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `auth_group_permissions`
--

INSERT INTO `auth_group_permissions` (`id`, `group_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(18, 1, 18),
(19, 1, 19),
(20, 1, 20),
(21, 1, 21),
(22, 1, 22),
(23, 1, 23),
(24, 1, 24),
(25, 1, 25),
(26, 1, 26),
(27, 1, 27),
(28, 1, 28),
(29, 1, 29),
(30, 1, 30),
(31, 1, 31),
(32, 1, 32),
(33, 1, 33),
(34, 1, 34),
(35, 1, 35),
(36, 1, 36),
(37, 1, 37),
(38, 1, 38),
(39, 1, 39),
(40, 1, 40),
(41, 1, 41),
(42, 1, 42);

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add main skill', 7, 'add_mainskill'),
(20, 'Can change main skill', 7, 'change_mainskill'),
(21, 'Can delete main skill', 7, 'delete_mainskill'),
(22, 'Can add sub skill', 8, 'add_subskill'),
(23, 'Can change sub skill', 8, 'change_subskill'),
(24, 'Can delete sub skill', 8, 'delete_subskill'),
(25, 'Can add user skill', 9, 'add_userskill'),
(26, 'Can change user skill', 9, 'change_userskill'),
(27, 'Can delete user skill', 9, 'delete_userskill'),
(28, 'Can add endorse', 10, 'add_endorse'),
(29, 'Can change endorse', 10, 'change_endorse'),
(30, 'Can delete endorse', 10, 'delete_endorse'),
(31, 'Can add review request', 11, 'add_reviewrequest'),
(32, 'Can change review request', 11, 'change_reviewrequest'),
(33, 'Can delete review request', 11, 'delete_reviewrequest'),
(34, 'Can add main skill criteria', 12, 'add_mainskillcriteria'),
(35, 'Can change main skill criteria', 12, 'change_mainskillcriteria'),
(36, 'Can delete main skill criteria', 12, 'delete_mainskillcriteria'),
(37, 'Can add user main skill rate', 13, 'add_usermainskillrate'),
(38, 'Can change user main skill rate', 13, 'change_usermainskillrate'),
(39, 'Can delete user main skill rate', 13, 'delete_usermainskillrate'),
(40, 'Can add manager review', 14, 'add_managerreview'),
(41, 'Can change manager review', 14, 'change_managerreview'),
(42, 'Can delete manager review', 14, 'delete_managerreview'),
(46, 'Can add notification', 16, 'add_notification'),
(47, 'Can change notification', 16, 'change_notification'),
(48, 'Can delete notification', 16, 'delete_notification'),
(49, 'Can add manager criteria rating', 17, 'add_managercriteriarating'),
(50, 'Can change manager criteria rating', 17, 'change_managercriteriarating'),
(51, 'Can delete manager criteria rating', 17, 'delete_managercriteriarating');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$12000$PG551KG3lXZK$44HLNzo8zunQZiuHcRg6r0G250GQLz2ZkufyWlufcYk=', '2015-07-14 12:43:16', 1, 'admin', '', '', '', 1, 1, '2015-07-02 12:58:07'),
(2, 'pbkdf2_sha256$12000$ex5BR2ht7Hiu$ntwOM+y9n26jRgufEDAYjcgfDzxxEOe0xMj99mECyHc=', '2015-07-14 15:41:58', 0, 'hari123', 'Hari', 'Shankar', 'hari.sayone@gmail.com', 0, 1, '2015-07-02 12:58:43'),
(3, 'pbkdf2_sha256$12000$tCIKBMj9sqHq$+xl7WDMp8or/4A5SNvklIehG6OjU/Unzr675U4Danaw=', '2015-07-14 13:08:44', 0, 'akhil123', 'Akhil', 'Jiji', 'akhil.sayone@gmail.com', 0, 1, '2015-07-02 12:59:01'),
(4, 'pbkdf2_sha256$12000$QV9w91oBTEdW$1EMKF9GiPQsg10+7kUmnr+6JU+gpPU824HBHHoJK9zE=', '2015-07-14 15:24:57', 0, 'sanoop123', 'Sanoop', 'P', 'sanoop.sayone@gmail.com', 0, 1, '2015-07-02 12:59:13'),
(5, 'pbkdf2_sha256$12000$5T8xEPB3KMGH$8VGV+KJPi1IoDKA26087Ef4zaVZHvK7zfoQIlB1s/BQ=', '2015-07-14 15:40:32', 0, 'ranju123', 'Ranju', 'Sayone', 'ranju.sayone@gmail.com', 0, 1, '2015-07-02 12:59:27'),
(6, 'pbkdf2_sha256$12000$K9ri0ZVCCSzb$yZgMiog4ntB10psrdomiIhfNC02f27LDjEUKHtCLEv8=', '2015-07-14 15:10:42', 0, 'ferose123', 'Ferose', 'Ali', 'ferose.sayone@gmail.com', 0, 1, '2015-07-02 12:59:42');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `auth_user_groups`
--

INSERT INTO `auth_user_groups` (`id`, `user_id`, `group_id`) VALUES
(4, 5, 1),
(5, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2015-07-02 12:58:43', '2', 'hari123', 1, '', 4, 1),
(2, '2015-07-02 12:58:49', '2', 'hari123', 2, 'No fields changed.', 4, 1),
(3, '2015-07-02 12:59:01', '3', 'akhil123', 1, '', 4, 1),
(4, '2015-07-02 12:59:13', '4', 'sanoop123', 1, '', 4, 1),
(5, '2015-07-02 12:59:27', '5', 'ranju123', 1, '', 4, 1),
(6, '2015-07-02 12:59:42', '6', 'ferose123', 1, '', 4, 1),
(7, '2015-07-02 13:00:06', '1', 'manager', 1, '', 3, 1),
(8, '2015-07-02 13:00:21', '5', 'ranju123', 2, 'Changed groups.', 4, 1),
(9, '2015-07-02 13:00:45', '3', 'akhil123', 2, 'Changed groups.', 4, 1),
(10, '2015-07-03 08:37:30', '1', 'Technical', 1, '', 7, 1),
(11, '2015-07-03 08:37:36', '2', 'Management', 1, '', 7, 1),
(12, '2015-07-03 08:37:40', '3', 'Arts', 1, '', 7, 1),
(13, '2015-07-03 08:37:44', '4', 'Sports', 1, '', 7, 1),
(14, '2015-07-03 08:38:50', '1', 'Django', 1, '', 8, 1),
(15, '2015-07-03 08:39:04', '2', 'Angular Js', 1, '', 8, 1),
(16, '2015-07-03 08:39:11', '3', 'Node Js', 1, '', 8, 1),
(17, '2015-07-03 08:39:18', '4', 'Php', 1, '', 8, 1),
(18, '2015-07-03 08:39:31', '5', 'Backbone Js', 1, '', 8, 1),
(19, '2015-07-03 08:39:39', '6', 'Knockout Js', 1, '', 8, 1),
(20, '2015-07-03 08:39:48', '7', 'Ruby On Rails', 1, '', 8, 1),
(21, '2015-07-03 08:39:54', '8', 'Java', 1, '', 8, 1),
(22, '2015-07-03 08:40:30', '1', 'Client Review', 1, '', 12, 1),
(23, '2015-07-03 08:40:46', '2', 'Code Optimization', 1, '', 12, 1),
(24, '2015-07-03 08:41:06', '3', 'Better Delivery Time', 1, '', 12, 1),
(25, '2015-07-03 08:41:33', '4', 'Latest technologies used', 1, '', 12, 1),
(26, '2015-07-03 08:41:46', '5', 'Code Reusability', 1, '', 12, 1),
(27, '2015-07-03 08:41:55', '6', 'Code Expandability', 1, '', 12, 1),
(28, '2015-07-03 08:42:18', '7', 'Coding Efficiency', 1, '', 12, 1),
(29, '2015-07-03 09:02:17', '2', 'Django', 1, '', 9, 1),
(30, '2015-07-03 09:07:20', '5', 'ranju123', 2, 'No fields changed.', 4, 1),
(31, '2015-07-03 09:08:02', '5', 'ranju123', 2, 'Changed first_name, last_name and email.', 4, 1),
(32, '2015-07-03 11:31:52', '3', 'Django', 1, '', 11, 1),
(33, '2015-07-03 11:32:05', '3', 'Django', 3, '', 11, 1),
(34, '2015-07-03 11:32:05', '2', 'Django', 3, '', 11, 1),
(35, '2015-07-07 05:07:48', '3', 'Angular Js', 1, '', 9, 1),
(36, '2015-07-07 05:07:55', '4', 'Backbone Js', 1, '', 9, 1),
(37, '2015-07-07 05:08:21', '4', 'Angular Js', 1, '', 11, 1),
(38, '2015-07-07 11:57:48', '5', 'Knockout Js', 1, '', 9, 1),
(39, '2015-07-07 11:58:04', '5', 'Knockout Js', 1, '', 11, 1),
(40, '2015-07-10 06:52:40', '7', 'UserSkill object', 1, '', 9, 1),
(41, '2015-07-10 06:57:16', '3', 'akhil123', 2, 'Changed groups.', 4, 1),
(42, '2015-07-10 06:57:25', '6', 'ferose123', 2, 'No fields changed.', 4, 1),
(43, '2015-07-14 05:46:33', '4', 'sanoop123', 2, 'Changed groups.', 4, 1),
(44, '2015-07-14 11:03:33', '18', 'ferose123 Review Request 57 minutes ago', 3, '', 16, 1),
(45, '2015-07-14 11:03:33', '17', 'akhil123 Review Request 1 hour, 46 minutes ago', 3, '', 16, 1),
(46, '2015-07-14 11:03:33', '16', 'hari123 Review Request 1 hour, 48 minutes ago', 3, '', 16, 1),
(47, '2015-07-14 11:03:33', '15', 'hari123 Review Request 1 hour, 48 minutes ago', 3, '', 16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_3ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`) VALUES
(1, 'log entry', 'admin', 'logentry'),
(2, 'permission', 'auth', 'permission'),
(3, 'group', 'auth', 'group'),
(4, 'user', 'auth', 'user'),
(5, 'content type', 'contenttypes', 'contenttype'),
(6, 'session', 'sessions', 'session'),
(7, 'main skill', 'skills', 'mainskill'),
(8, 'sub skill', 'skills', 'subskill'),
(9, 'user skill', 'skills', 'userskill'),
(10, 'endorse', 'skills', 'endorse'),
(11, 'review request', 'skills', 'reviewrequest'),
(12, 'main skill criteria', 'skills', 'mainskillcriteria'),
(13, 'user main skill rate', 'skills', 'usermainskillrate'),
(14, 'manager review', 'skills', 'managerreview'),
(16, 'notification', 'notifications', 'notification'),
(17, 'manager criteria rating', 'skills', 'managercriteriarating');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2015-07-02 12:56:03'),
(2, 'auth', '0001_initial', '2015-07-02 12:56:04'),
(3, 'admin', '0001_initial', '2015-07-02 12:56:04'),
(4, 'sessions', '0001_initial', '2015-07-02 12:56:04'),
(5, 'skills', '0001_initial', '2015-07-02 12:56:06'),
(6, 'skills', '0002_auto_20150703_1422', '2015-07-03 08:52:19'),
(7, 'skills', '0003_auto_20150703_1430', '2015-07-03 09:00:52'),
(8, 'notifications', '0001_initial', '2015-07-07 09:49:49'),
(9, 'notifications', '0002_auto_20150224_1134', '2015-07-07 09:49:50'),
(10, 'notifications', '0003_notification_data', '2015-07-07 09:49:50'),
(11, 'notifications', '0004_auto_20150707_1517', '2015-07-07 09:49:50'),
(12, 'skills', '0004_auto_20150707_1519', '2015-07-07 09:49:50'),
(13, 'skills', '0002_auto_20150708_1049', '2015-07-10 07:33:11'),
(14, 'skills', '0003_auto_20150708_1050', '2015-07-10 07:40:00'),
(15, 'skills', '0004_auto_20150709_1922', '2015-07-10 07:40:14'),
(16, 'skills', '0005_merge', '2015-07-10 07:40:14'),
(17, 'skills', '0006_auto_20150710_1303', '2015-07-10 07:40:14'),
(18, 'skills', '0007_managercriteriarating', '2015-07-10 14:29:43'),
(19, 'skills', '0008_managercriteriarating_rated_on', '2015-07-13 04:06:22'),
(20, 'skills', '0009_auto_20150713_1001', '2015-07-13 04:31:10'),
(21, 'skills', '0010_reviewrequest_requested_on', '2015-07-13 05:08:24'),
(22, 'skills', '0011_auto_20150713_1047', '2015-07-13 05:17:17'),
(23, 'skills', '0012_auto_20150713_1440', '2015-07-13 09:10:45'),
(24, 'skills', '0013_auto_20150714_1127', '2015-07-14 05:58:09'),
(25, 'skills', '0014_auto_20150714_1215', '2015-07-14 06:45:09'),
(26, 'skills', '0012_auto_20150713_1805', '2015-07-14 09:01:42'),
(27, 'skills', '0013_auto_20150714_0933', '2015-07-14 09:01:43'),
(28, 'skills', '0015_merge', '2015-07-14 09:01:43'),
(29, 'skills', '0016_auto_20150714_1431', '2015-07-14 09:01:44'),
(30, 'skills', '0017_auto_20150714_1441', '2015-07-14 09:11:40'),
(31, 'skills', '0018_auto_20150714_1442', '2015-07-14 09:13:28');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('v0tr8cdh5bhm235seb99437bd27qolgl', 'NjUxYmZkMzkwOGU0NjlhODg4ZWQxZjgzMzAxMzNiNTU4ZTU3MTJiYTp7fQ==', '2015-07-16 13:01:21'),
('z4nkbpcobsvhtag5cdhb0x6mne1rx178', 'NjUxYmZkMzkwOGU0NjlhODg4ZWQxZjgzMzAxMzNiNTU4ZTU3MTJiYTp7fQ==', '2015-07-22 06:54:55'),
('66mjxshigie3gyifljnnhascsql47t92', 'M2UzODEwNWU4NjQ2NTk3NzEyMDIzYzAxNTdmZGU0MTZkM2ZmNmZjZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiNTFlZDRlZDc3MGJmMjlkYmVhYWViZmI1MDJkN2NlM2ZhMDhiZDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9', '2015-07-28 15:41:58');

-- --------------------------------------------------------

--
-- Table structure for table `notifications_notification`
--

CREATE TABLE IF NOT EXISTS `notifications_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(20) NOT NULL,
  `unread` tinyint(1) NOT NULL,
  `actor_object_id` varchar(255) NOT NULL,
  `verb` varchar(255) NOT NULL,
  `description` longtext,
  `target_object_id` varchar(255) DEFAULT NULL,
  `action_object_object_id` varchar(255) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `public` tinyint(1) NOT NULL,
  `action_object_content_type_id` int(11) DEFAULT NULL,
  `actor_content_type_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `target_content_type_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `emailed` tinyint(1) NOT NULL,
  `data` longtext,
  PRIMARY KEY (`id`),
  KEY `notifications_notification_142874d9` (`action_object_content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `notifications_notification`
--

INSERT INTO `notifications_notification` (`id`, `level`, `unread`, `actor_object_id`, `verb`, `description`, `target_object_id`, `action_object_object_id`, `timestamp`, `public`, `action_object_content_type_id`, `actor_content_type_id`, `recipient_id`, `target_content_type_id`, `deleted`, `emailed`, `data`) VALUES
(19, 'info', 1, '6', 'Review Request', 'I had sent a request to rate my current skill', NULL, NULL, '2015-07-14 14:34:38', 1, NULL, 4, 5, NULL, 0, 0, NULL),
(20, 'info', 1, '2', 'Review Request', 'I had sent a request to rate my current skill', NULL, NULL, '2015-07-14 14:42:32', 1, NULL, 4, 5, NULL, 0, 0, NULL),
(21, 'info', 1, '2', 'Review Request', 'I had sent a request to rate my current skill', NULL, NULL, '2015-07-14 15:24:40', 1, NULL, 4, 4, NULL, 0, 0, NULL),
(22, 'info', 1, '2', 'Review Request', 'I had sent a request to rate my current skill', NULL, NULL, '2015-07-14 15:24:44', 1, NULL, 4, 4, NULL, 0, 0, NULL),
(23, 'info', 1, '2', 'Review Request', 'I had sent a request to rate my current skill', NULL, NULL, '2015-07-14 15:40:10', 1, NULL, 4, 5, NULL, 0, 0, NULL),
(24, 'info', 1, '2', 'Review Request', 'I had sent a request to rate my current skill', NULL, NULL, '2015-07-14 15:40:14', 1, NULL, 4, 5, NULL, 0, 0, NULL),
(25, 'info', 1, '2', 'Review Request', 'I had sent a request to rate my current skill', NULL, NULL, '2015-07-14 15:40:18', 1, NULL, 4, 5, NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `skills_endorse`
--

CREATE TABLE IF NOT EXISTS `skills_endorse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endorse_date` datetime NOT NULL,
  `added_by_id` int(11) NOT NULL,
  `user_skill_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skills_endorse_0c5d7d4e` (`added_by_id`),
  KEY `skills_endorse_714e3549` (`user_skill_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `skills_endorse`
--

INSERT INTO `skills_endorse` (`id`, `endorse_date`, `added_by_id`, `user_skill_id`) VALUES
(3, '2015-07-14 15:09:43', 2, 23);

-- --------------------------------------------------------

--
-- Table structure for table `skills_mainskill`
--

CREATE TABLE IF NOT EXISTS `skills_mainskill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(100) NOT NULL,
  `added_on` datetime NOT NULL,
  `skill_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `skills_mainskill`
--

INSERT INTO `skills_mainskill` (`id`, `skill_name`, `added_on`, `skill_description`) VALUES
(1, 'Technical', '2015-07-03 08:37:30', ''),
(2, 'Management', '2015-07-03 08:37:36', ''),
(3, 'Arts', '2015-07-03 08:37:40', ''),
(4, 'Sports', '2015-07-03 08:37:44', '');

-- --------------------------------------------------------

--
-- Table structure for table `skills_mainskillcriteria`
--

CREATE TABLE IF NOT EXISTS `skills_mainskillcriteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criteria` varchar(144) NOT NULL,
  `main_skill_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skills_mainskillcriteria_40ee53de` (`main_skill_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `skills_mainskillcriteria`
--

INSERT INTO `skills_mainskillcriteria` (`id`, `criteria`, `main_skill_id`) VALUES
(1, 'Client Review', 1),
(2, 'Code Optimization', 1),
(3, 'Better Delivery Time', 1),
(4, 'Latest technologies used', 1),
(5, 'Code Reusability', 1),
(6, 'Code Expandability', 1),
(7, 'Coding Efficiency', 1);

-- --------------------------------------------------------

--
-- Table structure for table `skills_managercriteriarating`
--

CREATE TABLE IF NOT EXISTS `skills_managercriteriarating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `starrate` int(11) NOT NULL,
  `byuser_id` int(11) NOT NULL,
  `mcriteria_id` int(11) NOT NULL,
  `uskill_id` int(11) NOT NULL,
  `rated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skills_managercriteriarating_9eaf27ba` (`byuser_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=314 ;

--
-- Dumping data for table `skills_managercriteriarating`
--

INSERT INTO `skills_managercriteriarating` (`id`, `starrate`, `byuser_id`, `mcriteria_id`, `uskill_id`, `rated_on`) VALUES
(293, 3, 5, 1, 27, '2015-07-14 15:40:50'),
(294, 4, 5, 2, 27, '2015-07-14 15:40:50'),
(295, 5, 5, 3, 27, '2015-07-14 15:40:50'),
(296, 5, 5, 4, 27, '2015-07-14 15:40:50'),
(297, 5, 5, 5, 27, '2015-07-14 15:40:50'),
(298, 5, 5, 6, 27, '2015-07-14 15:40:50'),
(299, 5, 5, 7, 27, '2015-07-14 15:40:50'),
(307, 3, 5, 1, 29, '2015-07-14 15:41:20'),
(308, 5, 5, 2, 29, '2015-07-14 15:41:20'),
(309, 3, 5, 3, 29, '2015-07-14 15:41:20'),
(310, 3, 5, 4, 29, '2015-07-14 15:41:20'),
(311, 4, 5, 5, 29, '2015-07-14 15:41:20'),
(312, 5, 5, 6, 29, '2015-07-14 15:41:20'),
(313, 4, 5, 7, 29, '2015-07-14 15:41:20');

-- --------------------------------------------------------

--
-- Table structure for table `skills_managerreview`
--

CREATE TABLE IF NOT EXISTS `skills_managerreview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(144) NOT NULL,
  `comment` varchar(250) NOT NULL,
  `average_rate` double NOT NULL,
  `added_by_id` int(11) NOT NULL,
  `sub_skill_id` int(11) NOT NULL,
  `user_id` int(11),
  PRIMARY KEY (`id`),
  KEY `skills_managerreview_0c5d7d4e` (`added_by_id`),
  KEY `skills_managerreview_eeecebf1` (`sub_skill_id`),
  KEY `skills_managerreview_3b029f5a` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `skills_managerreview`
--

INSERT INTO `skills_managerreview` (`id`, `project_name`, `comment`, `average_rate`, `added_by_id`, `sub_skill_id`, `user_id`) VALUES
(84, 'dfghgh', 'dghtrhr', 7.7, 5, 7, 2),
(82, 'thge', 'gfdhg', 9.1, 5, 8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `skills_reviewrequest`
--

CREATE TABLE IF NOT EXISTS `skills_reviewrequest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `to_user_id` int(11) NOT NULL,
  `user_skill_id` int(11) NOT NULL,
  `requested_on` datetime,
  PRIMARY KEY (`id`),
  KEY `skills_reviewrequest_63add04c` (`to_user_id`),
  KEY `skills_reviewrequest_714e3549` (`user_skill_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `skills_reviewrequest`
--

INSERT INTO `skills_reviewrequest` (`id`, `status`, `to_user_id`, `user_skill_id`, `requested_on`) VALUES
(30, '1', 5, 27, '2015-07-14 15:40:18'),
(28, '1', 5, 29, '2015-07-14 15:40:10');

-- --------------------------------------------------------

--
-- Table structure for table `skills_subskill`
--

CREATE TABLE IF NOT EXISTS `skills_subskill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subskill_name` varchar(100) NOT NULL,
  `added_on` datetime NOT NULL,
  `subskill_description` varchar(200) DEFAULT NULL,
  `mainskill_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skills_subskill_9c11403c` (`mainskill_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `skills_subskill`
--

INSERT INTO `skills_subskill` (`id`, `subskill_name`, `added_on`, `subskill_description`, `mainskill_id`) VALUES
(1, 'Django', '2015-07-03 08:38:50', '', 1),
(2, 'Angular Js', '2015-07-03 08:39:04', '', 1),
(3, 'Node Js', '2015-07-03 08:39:11', '', 1),
(4, 'Php', '2015-07-03 08:39:18', '', 1),
(5, 'Backbone Js', '2015-07-03 08:39:31', '', 1),
(6, 'Knockout Js', '2015-07-03 08:39:39', '', 1),
(7, 'Ruby On Rails', '2015-07-03 08:39:48', '', 1),
(8, 'Java', '2015-07-03 08:39:54', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `skills_usermainskillrate`
--

CREATE TABLE IF NOT EXISTS `skills_usermainskillrate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `overall_skill_rate` double NOT NULL,
  `main_skill_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skills_usermainskillrate_40ee53de` (`main_skill_id`),
  KEY `skills_usermainskillrate_e8701ad4` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `skills_usermainskillrate`
--

INSERT INTO `skills_usermainskillrate` (`id`, `overall_skill_rate`, `main_skill_id`, `user_id`) VALUES
(13, 0.27, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `skills_userskill`
--

CREATE TABLE IF NOT EXISTS `skills_userskill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `added_date` datetime NOT NULL,
  `endorse_count` int(11) NOT NULL,
  `avg_skill_rate` double NOT NULL,
  `sub_skill_id` int(11) NOT NULL,
  `user_id` int(11),
  PRIMARY KEY (`id`),
  KEY `skills_userskill_eeecebf1` (`sub_skill_id`),
  KEY `skills_userskill_3b029f5a` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `skills_userskill`
--

INSERT INTO `skills_userskill` (`id`, `added_date`, `endorse_count`, `avg_skill_rate`, `sub_skill_id`, `user_id`) VALUES
(29, '2015-07-14 15:40:07', 0, 7.7, 7, 2),
(27, '2015-07-14 15:39:53', 0, 9.1, 8, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
