#import notifications

from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^', include('apps.accounts.urls')),
    url(r'^skills/', include('apps.skills.urls')),
    url(r'^admin/', include(admin.site.urls)),
   #url(r'^inbox/notifications/', include(notifications.urls))
)